Mesh the Target Geometry
===============================

.. rst-class:: step-number

1\. Run snappyHexMesh

For this example, we can again use the default settings to generate a relatively coarse mesh of the Ahmed Body inside the wind tunnel. In the control view at the bottom section of the tab, click the :guilabel:`Run snappyHexMesh` button to launch the OpenFOAM snappyHexMesh utility. Meshing can take several minutes, depending on the system. Wait for the dialog to show the "Finished meshing" information followed by a line with the word "End", then close the dialog.


.. rst-class:: step-number

2\. View the Mesh

To see the results, click the :guilabel:`Load Data` button in the control view, and then click the :guilabel:`Hide Others` button. If you zoom inside the wind tunnel, you can see the mesh elements generated at the surface of the Ahmed Body. This mesh would typically be considered very coarse by production standards, but is sufficient for this example.

.. image:: ../images/ahmedbody-snappyHexMesh.png
    :align: center

|

.. note::
  Just a reminder that we are taking some shortcuts here in order to keep the computing time relatively short for demonstration purposes. OpenFOAM can generate meshes of much higher quality, and can also be run in parallel to significantly reduce meshing time.

This completes the meshing tasks; now we can use the results to compute fluid flow.
