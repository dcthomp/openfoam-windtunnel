Import the Target Geometry
============================

.. |nbsp| unicode:: 0xa0
    :trim:

For this example, we will again start with the :guilabel:`Model` tab to import our target geometry and run OpenFOAM's :program:`surfaceFeatureExtract` program to detect model edges. We will follow the same basic steps that you did for the wing section example.


.. rst-class:: step-number

1\. Download Model File

Download the :file:`AhmedBody.stl` file using the following link:

:download:`AhmedBody.stl <../_downloads/AhmedBody.stl>` |nbsp| |nbsp| (right click on the link and use the ":menuselection:`Save Link As...`" option)


.. rst-class:: step-number

2\. Import Into ModelBuilder

In the :guilabel:`Import Model` section, click the :guilabel:`Run` button to bring up the dialog for loading the model file. Click the :guilabel:`Browse` button and navigate to the :file:`AhmedBody.stl` file you downloaded. When that is set, click the dialog's :guilabel:`Apply` button. In response, :program:`modelbuilder` will read the file, create an internal representation for the geometry and topology, and display the result in the 3D view.

.. image:: ../images/ahmedbody-model-loaded.png
    :align: center

|

.. rst-class:: step-number

3\. Surface Features

In the :guilabel:`Surface Features` section, we need to make one edit:

Set the :guilabel:`Inside Model Point` to  ``0.5``   ``0.1``   ``0.0``

.. rst-class:: step-number

4\. Run surfaceFeatureExtract

In the bottom section, click the :guilabel:`Run surfaceFeatureExtract` button. In response, :program:`modelbuilder` will set up and launch :program:`surfaceFeatureExtract` program and display its log file in a popup dialog. The run should only take a few seconds (unless you have not yet loaded the :program:`OpenFOAM` docker image, which will take minutes).

At this point, the modeling geometry is set up and you can close the log file dialog.

Before moving on, also select the :menuselection:`WindTunnel --> Save Project` menu item to save the changes to your project so far.

Then proceed to the :guilabel:`Block Mesh` tab where we will set the wind tunnel geometry.
