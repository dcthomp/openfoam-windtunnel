Display the Pressure Field
=============================

.. |paraview_off| image:: ../images/postprocessing-mode_off@2x.png
    :width: 24px

.. |reset_range| image:: ../images/pqResetRange.svg
    :width: 24px

.. |slice| image:: ../images/pqSlice.svg
     :width: 24px


.. rst-class:: step-number

1\. Load the Data

Load the pressure data similar to how you did for the wing section example:

  * Click the :guilabel:`Load Data` button in the control view, and then click the :guilabel:`Hide Others` button.
  * Click the "Enable ParaView" toolbar button ( |paraview_off| ) to make the :program:`ParaView` toolbars and filters visible.
  * This time, don't close the :guilabel:`Pipeline Browser` panel but instead undock it and drag/drop it onto the :guilabel:`OpenFOAM` so that it is displayed as a sidebar tab.
  * To see the final time step, find the dropdown box labeled :guilabel:`Time` in the ParaView toolbars and change it from  ``0.0`` to ``0.5``.
  * To update the color map to the 0.5 sec data, find and click the "Rescale to Data Range" button ( |reset_range| ) in the toolbar area.

The result will be a fairly uninteresting view of the wind tunnel outside surface. (You can also zoom inside the wind tunnel to see the pressure field on the surface of the Ahmed Body, which is slightly more interesting.)

.. image:: ../images/ahmedbody-pressure-0.5sec.png
    :align: center

|

.. note::
  The high pressure (bright red) region at the lower front of the wind tunnel is due to the discontinuity between the inlet velocity (1.0 m/sec) and the stationary floor. In production, we would use a function for the inlet velocity and keep C0 continuity at the floor.

To explore the dataset further, we can take advantage of :program:`ParaView`s` rich set of visualization features. To demonstrate one of them, we will add a slice plane next.


.. rst-class:: step-number

2\. Add a Slice Plane

To view the pressure field around the Ahmed Body, add a slice plane to the display by clicking the "Slice" toolbar button ( |slice| ). You can also use the :menuselection:`Filters --> Common --> Slice` menu item.

When the slice plane is displayed, we need to reorient it using the :guilabel:`Properties` view. If the properties view is not visible, enable it in the :menuselection:`View` menu.

The :guilabel:`Properties` view has three collapsible sections :guilabel:`Properties (Slice1)`, :guilabel:`Display (GeometryRepresentation)`, :guilabel:`View (Render View)`. Close the second and third sections, and in the :guilabel:`Properties (Slice1)` section, find the :guilabel:`Plane Parameters` subsection. There are a number of controls in this section; find and click the :guilabel:`Z Normal` button to align the slice plane's normal to be perpendicular to the flow direction. You can grab and drag the red-colored ring to move the plane in the Z direction, and grab the arrow in the center of the widget to adjust the angle. For now, just hide the widget by unchecking the :guilabel:`Show Plane` box in the properties view. You should be able to see the pressure spike at the front of the body and negative pressure at points on the top surface of the body.

.. image:: ../images/ahmedbody-pressure-slice.png
    :align: center

|
