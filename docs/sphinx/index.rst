#################
CMB Wind Tunnel
#################

.. image:: ./images/wingsection-stream.png
    :align: center

|

`Kitware <https://kitware.com>`_'s  `Computational ModelBuilder (CMB) <https://computationalmodelbuilder.org/>`_ platform is designed to support the full numerical simulation life cycle --- from preprocessing to execution to postprocessing --- by pulling together software tools and simulation codes into a cohesive end-to-end framework. Customization is implemented by template and script files so that the same runtime code can be used for diverse applications. CMB implementations have been successfully developed this way for many technical domains including computational fluid dynamics, heat transfer, high-energy physics, hydrology, nuclear physics, and wave energy conversion.

To help new users become familiar with :program:`CMB`, we put together this short tutorial using a customized version of the :program:`CMB modelbuilder` application. This tutorial will construct and carry out basic computational fluid dynamics (CFD) simulations with the popular `OpenFOAM <https://www.openfoam.com/>`_ simulation code. Instructions are provided for downloading the :program:`CMB modelbuilder` software and walking through example problems simulating wind tunnel physics. We have purposely kept the examples simple in order to focus on the features and benefits that :program:`CMB` provides for these kinds of workflows as well as not requiring the reader to have in depth knowledge of :program:`OpenFOAM`. We have also taken some shortcuts to save compute cycles.

Those familiar with :program:`OpenFOAM` know that it is a powerful analysis tool with an extensive feature set. Creating and editing the numerous text files --- needed, for example, to generate discrete geometries (meshes), specify  problem physics and boundary conditions, setup the time-step logic, etc. --- can be challenging. With :program:`modelbuilder`, these input files are generated automatically so that you don't have to deal with typos, syntax, or looking up keywords. The :program:`modelbuilder` application also organizes the workflow, storing the :program:`OpenFOAM` files in a project directory, so that users do not have to remember or lookup file system paths.

After completing these examples, you are welcome to experiment with various options in the user interface or try your own model files (``.obj``, ``.stl``, or ``.stlb``). We also added a :ref:`How It All Works` section to briefly describe how we are able to deploy CMB across many diverse simulation codes without writing new software from the ground up each time. :program:`CMB` is not based on :program:`OpenFOAM` or focused on CFD problems alone. Because of its designed-in flexibility, :program:`CMB` is ideal and cost efficient for a wide range of simulation codes and workflows. So if you are interested in using :program:`CMB` and :program:`modelbuilder` for your simulation workflows, please get in touch with us at https://www.kitware.com/contact/.



.. toctree::
   :maxdepth: 1

   install.rst
   wing-section/index.rst
   ahmed-body/index.rst
   how-it-works/index.rst

.. note::
  All of the :program:`CMB` source code used in :program:`CMB Wind Tunnel` is developed with an open source, permissive license. The :program:`CMB` source code is publicly available at https://gitlab.kitware.com/.
