.. For some reason, the title doesn't work as a reference (dont know why) so use a reference
.. _How It All Works:

How It All Works
==================

We hope the examples show some of the benefits of using the :program:`CMB` platform for simulation workflows, compared to the typical process of writing and organizing input decks by hand. For those wondering how we can apply :program:`CMB` across many technical disciplines (computational fluid dynamics, heat transfer, high-energy physics, etc.) without developing each application from scratch, the short answer is that we designed :program:`CMB`'s core software to use the same runtime code across every application. More details about :program:`CMB`'s core software --- called the :program:`Simulation Modeling Toolkit (SMTK)` --- can be found at https://smtk.readthedocs.io. Here is a brief overview of how we put the :program:`CMB Wind Tunnel` system together.

.. note::
    There are **many other** CMB/SMTK features that are not covered in this demonstration. If you want to know more, see the documentation at https://smtk.readthedocs.io or contact us at https://www.kitware.com/contact/.


.. rst-class:: step-number

1\. Declarative user interface

We designed :program:`CMB` with the flexibility to generate the user interface at runtime from text file input (XML format). So for this application, we authored text files enumerating the traits (names, types, value ranges, options and so forth) for the relevant OpenFOAM mesh settings, boundary conditions, material models, convergence settings, output options, etc. The file syntax includes a section where we describe how to display the various features and organize them into tabs and other groupings in the user interface. These *simulation workflow* files are installed with the :program:`CMB Wind Tunnel` package, so that when the user creates a new project, :program:`modelbuilder` reads them and produces the user interface you see in the :guilabel:`OpenFOAM` panel.


.. rst-class:: step-number

2\. Attribute Resource

When the user edits the fields in the user interface, we store the data in an internal representation we call an *attribute resource*. :program:`CMB` includes software to read and write attribute resources to the file system, so that :program:`modelbuilder` can populate the user interface at startup and save changes.


.. rst-class:: step-number

3\. Python API

To write :program:`OpenFOAM` input files, we authored :program:`Python` scripts that are installed with :program:`CMB Wind Tunnel`. These scripts access the data in the attribute resource using the :program:`CMB` API. For most :program:`CMB` software: (i) source code is written in :program:`C++` for performance, and (ii) :program:`Python` bindings are added to each class for script access. Our :program:`Python` scripts essentially traverse the contents of the attribute resource and, using the values entered by the user, write the :program:`OpenFOAM` input files.


.. rst-class:: step-number

4\. Plugin

In the interest of full disclosure, we also added some custom code to :program:`CMB Wind Tunnel` in the form of a :program:`C++` plugin to provide two features that are not yet available in the standard :program:`CMB` runtime: (i) the menu for project functions (new, open, save, close) and (ii) the control views that are used in each tab. This was a relatively modest effort to improve usability. If you want to see the plugin source code, it is at https://gitlab.kitware.com/cmb/plugins/openfoam-windtunnel.


.. rst-class:: step-number

Summary

By designing :program:`CMB` with this flexibility, we can provide a productive preprocessing user interface for a relatively modest development effort. And because :program:`CMB` is built on top of the :program:`ParaView` platform, we can easily integrate postprocessing and visualization into the same application. So if you want to create a professional user interface for your simulation code,  contact us at https://www.kitware.com/contact/.
