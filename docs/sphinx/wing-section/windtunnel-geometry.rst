Set the Wind Tunnel Geometry
===============================

.. |reset_camera| image:: ../images/pqResetCamera.svg
    :width: 24


The next step is to setup the geometry of a virtual wind tunnel to surround the wing section model.

.. rst-class:: step-number

1\. Select the Block Mesh tab

When you click on the :guilabel:`Block Mesh` tab, the 3D :guilabel:`RenderView` updates to display the wing section object along with the outline of an axis-aligned rectangular solid that represents the geometry to use for the wind tunnel. To see the full geometry, you can use your mouse's scroll button to zoom the view out. You can also click the "Reset Camera" button ( |reset_camera| ) to zoom to a position where you can see all of the displayed geometry.

.. image:: ../images/toolbar-reset-camera.png
    :align: center

|


.. rst-class:: step-number

2\. Adjust the geometry

The rectangular outline is an interactive widget that you can resize and reshape by dragging the small spherically-shapped grabbers. We want to do two things: (i) extend the box in the negative Y direction so that the wing section is centered in Y, and (ii) reduce the size in the Z direction because the wing cross section is uniform and, therefore, the calculated fields are uniform in that direction.

When you adjust the widget size/shape in the :guilabel:`RenderView`, you will see the numerical fields in the :guilabel:`Block Mesh` tab update, specifically next to the :guilabel:`Position` and :guilabel:`Scale` labels. These fields are kept in sync (bidirectionally) with the widget. For this example, enter these numbers in the :guilabel:`Geometry` section:

::

    Position: -1,  -1,  -0.1
    Scale:     3    2    0.2

.. rst-class:: step-number

3\. Edit the other blockMesh inputs

Below the :guilabel:`Geometry` section is a :guilabel:`Mesh Size` section with one field with the same name. We can specify the mesh size in several ways. Leave the default settings as is; they will generate a 3D grid with 100 elements in the longest direction (X in this project) and internally set a mesh size in the other coordinate directions to produce voxels with (approximately) equal edge lengths.

Below that is a :guilabel:`Boundary Conditions` section for assigning the walls of the wind tunnel. Our example always uses the "Left" side (YZ plane at min X) as the inlet surface and the "Right" side (YZ plane at max X) as the outlet surface, so these are not included in the user interface. The :guilabel:`Front and Back Sides` are the XY planes at min/max Z and, for our application, are always set to be the same. The :guilabel:`Top Side` is the XZ plane at max Y and the :guilabel:`Bottom Side` is the XZ plane at min Y.

For the wing section example, we only need to update one field: select the dropdown list next to  and select the :code:`Symmetry Plane` value.

.. image:: ../images/wingsection-blockwidget.png
    :align: center

|

At this point, save your work using the :menuselection:`WindTunnel --> Save Project` menu item.

.. rst-class:: step-number

4\. Run blockMesh

The bottom section of the tab is a control view labeled :guilabel:`blockMesh`, which is used to run the OpenFOAM application with the same name. The blockMesh utility decomposes the rectangular geometry specified for the wind tunnel into a grid of three-dimensional, hexahedral blocks. The blockMesh utility can construct meshes with features such as grading and curved edges, but our example works fine with a linear edges and a uniform grid.

Click the :guilabel:`Run blockMesh` button to launch the OpenFOAM blockMesh utility. Modelbuilder will display the log messages in a pop up dialog. The process should only take a few seconds, and you can close the dialog when you see the mesh information followed by a line with the word "End".

To see the results of the blockMesh run, click the :guilabel:`Load Data` button in the control view, which will cause modelbuilder to load and display the mesh geometry.

.. image:: ../images/wingsection-blockMesh.png
    :align: center

|

With this step complete, we can skip the :guilabel:`Refine Mesh` tab and go straight to the :guilabel:`Snappy Hex Mesh` tab next.
