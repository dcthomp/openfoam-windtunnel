Display the Pressure Field
=============================

.. |paraview_off| image:: ../images/postprocessing-mode_off@2x.png
    :width: 24px

.. |reset_range| image:: ../images/pqResetRange.svg
    :width: 24px

The first results data we will display is the pressure field computed by the :program:`icoFoam` solver.


.. rst-class:: step-number

1\. Load the Data

To see the results, click the :guilabel:`Load Data` button in the control view, and then click the :guilabel:`Hide Others` button. This will display the pressure field computed at the initial time step (0.0) as surface data colored by value. In order to see the final pressure field (0.5 sec) computed by :program:`icoFoam`, we need to first enable the :program:`ParaView` visualization features.


.. rst-class:: step-number

2\. Enable ParaView Features

By default, :program:`modelbuilder` hides the :program:`ParaView` :guilabel:`Pipeline Browser` and related toolbars and filters. To make them visible, find and click the "Enable ParaView" toolbar button ( |paraview_off| ).

.. image:: ../images/toolbar-paraview.png
    :align: center

|

Once clicked, the button will change to the standard :program:`ParaView` red-green-blue colors, the Pipeline Browser will be displayed in the sidebar, and several additional ParaView toolbars will be displayed. To conserve screen space, close the :guilabel:`Pipeline Browser` panel (we won't be using it). Alternatively, you can also undock the :guilabel:`Pipeline Browser` panel and drag/drop it over the :guilabel:`OpenFOAM` panel to display it as a tabbed panel.


.. rst-class:: step-number

3\. View Final Time step

The pressure field displayed by default is that initial one computed for time 0.0. To see the final time step, find the dropdown box labeled :guilabel:`Time` in the ParaView toolbars and change it from :guilabel:`0.0` to :guilabel:`0.5`.

.. image:: ../images/wingsection-pressure-0.5sec.png
    :align: center

|


.. rst-class:: step-number

4\. Rescale the Colors

When the data was loaded, the color map in the 3D view was automatically scaled to the range of pressure values in the time 0.0 sec data. To update the color map to the 0.5 sec data, find and click the "Rescale to Data Range" button ( |reset_range| ) in the toolbar area.

.. image:: ../images/toolbar-rescale-colors.png
    :align: center

|
