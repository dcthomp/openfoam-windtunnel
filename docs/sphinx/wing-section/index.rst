.. _Example Wing Section:


Example: Wing Section
=======================

.. image:: ../images/wingsection-stream.png
    :align: center

|

The first example uses :program:`modelbuilder` and :program:`OpenFOAM` to compute steady, laminar, incompressible flow around a geometric model of an airplane wing. For the model, we are using a wing section file that is included in the :program:`OpenFOAM` code repository. (There is a download link on the next page.) This example goes through the steps to create an :program:`OpenFOAM` project, import the model file, generate a background mesh, generate the analysis mesh, run the :program:`icoFoam` solver, and view the computed results.

.. toctree::
    :maxdepth: 1

    create-project.rst
    import-geometry.rst
    windtunnel-geometry.rst
    snappy-hex-mesh.rst
    icofoam.rst
    display-pressure.rst
    display-velocity.rst
    close-project.rst
    under-the-hood.rst

.. note::
    Screenshots in this section were taken from :program:`modelbuilder` running on a linux (Ubuntu) desktop machine.
