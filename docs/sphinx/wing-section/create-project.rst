Create a Wind Tunnel Project
==============================

.. figure:: ../images/windtunnel-menu.png
    :figwidth: 40%
    :align: right

Most of the menu interactions will be with the :guilabel:`WindTunnel` project menu, which has items to open existing :program:`modelbuilder` projects, create new projects, save projects, and close projects.

To begin, select the :menuselection:`WindTunnel --> New Project...` menu item.  In response, :program:`modelbuilder` displays a dialog for specifying a directory for storing persistent data. Use the :guilabel:`Browse` button to open a file system browser and navigate to convenient location and create a new directory there. A suggested location is :file:`${HOME}/modelbuilder/projects/wingsection`. After creating and selecting the directory, return to the :guilabel:`New Project` dialog and click the :guilabel:`Apply` button.

.. image:: ../images/new-project-dialog.png
    :align: center

|

.. note::
  You can use an existing directory if you check the ``OK to overwrite existing directory?`` box. If you do that, :program:`modelbuilder` will delete the directory's previous contents when you click the :guilabel:`Apply` button.

When the dialog closes, :program:`modelbuilder` creates an empty project and the :guilabel:`OpenFOAM` dockable widget (in the sidebar) will display a set of tabs for user input. In brief:

* The :guilabel:`Model` tab is for importing the model geometry.
* The :guilabel:`Block Mesh` tab is for setting the wind tunnel geometry and generating the initial background mesh.
* The :guilabel:`Refine Mesh` tab is for optionally adding refinement regions to the background mesh.
* The :guilabel:`Snappy Hex Mesh` tab if for generating the analysis mesh.
* The :guilabel:`Solver` tab is for specifying the physics and running the solver.

.. image:: ../images/project-created.png
    :align: center

|

We will be traversing these tabs from left to right in the next pages.
