Installing the software
========================

.. TODO If you have OpenFOAM binaries installed on your local machine, we have a hack that will let you use your installed version instead of openfoam-docker. See LINK


1\. Install Container Engine
------------------------------

To facilitate accessing :program:`OpenFOAM` in these examples, we will be using precompiled *Docker images* that are made available to the OpenFOAM community. This will avoid having to do a binary install and make sure that all users have the same version. (This will also make it easy to uninstall OpenFOAM by deleting the docker image.)

If you don't have a container engine installed on your system, you will need to do that first. There are several options:

1.1 Docker container engine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On most Linux machines, the package manager includes the Docker container engine and command line interface (CLI) as standard packages. If available, this is the easiest way to install a container engine on your system.

1.2 Docker Desktop
^^^^^^^^^^^^^^^^^^^^^

A second alternative is :program:`Docker Desktop` which is available at https://www.docker.com/products/docker-desktop/. Note that a subscription service agreement is required to use :program:`Docker Desktop` and a subscription fee is required for businesses over a certain size.

1.3 Podman
^^^^^^^^^^^^^^

A third alternative is :program:`Podman`, an open source tool for creating, running, and maintaining containers and container images. It provides a CLI compatible with the Docker container engine. :program:`Podman` installation instructions are at https://podman.io/getting-started/installation.

Note that, after installing :program:`Podman`, you must set the podman machine "rootful" option before starting the podman machine:

.. code:: bash

    > podman machine init
    > podman machine set --rootful
    > podman machine start


2\. Download modelbuilder Package
-----------------------------------

Modelbuilder release packages can be downloaded from this online folder at `data.kitware.com <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/63505dca11dab81428209490>`_. Find the package name for your OS; at the time of this writing, the choices are:

* :file:`modelbuilder-windtunnel-centos7-221209.tar.gz` for linux machines
* :file:`modelbuilder-windtunnel-macos-arm64-11.0-221208.dmg` for macOS/ARM64 machines
* :file:`modelbuilder-windtunnel-macos-x86_64-10.14-221209.dmg` for macOS/Intel machines
* :file:`modelbuilder-windtunnel-windows-221209.zip` for Windows machines

For linux and Windows machines, unpack the file in a convenient location. The :program:`modelbuilder` executable is in the :file:`bin` directory. When you start modelbuilder for the first time on Windows, the system will display a blue popup with the message "Windows protected your PC". To continue, find and click the :guilabel:`More info` link, then click the :guilabel:`Run anyway` button.

.. image:: images/windows-defender-popup.png
    :align: right
    :width: 300

For macOS machines, open the :file:`.dmg` file and drag the modelbuilder application to a convenient location in your file system. You might have some popup dialogs requesting permission to access parts of your file system from the application. Be sure to answer in the affirmative.

When you first start :program:`modelbuilder`, it looks similar to the :program:`ParaView` desktop application, with the main differences being that (i) many of the ParaView toolbars have been hidden, and (ii) the left sidebar has two new dock widgets labeled :guilabel:`Resources` and :guilabel:`Attribute Editor`, and (iii) there is a new :guilabel:`Wind Tunnel` menu.

.. image:: images/modelbuilder-start.png
    :align: center

|
