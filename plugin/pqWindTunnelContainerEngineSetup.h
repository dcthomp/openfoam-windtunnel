//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelContainerEngineSetup_h
#define plugin_pqWindTunnelContainerEngineSetup_h

#include <QObject>

#include <QProcess>
#include <QString>

class QWidget;

class qtProgressDialog;

/**\brief pqWindTunnelContainerEngineSetup
 *
 * Handles the UI logic for configuring the container engine
 * and fetching the openfoam docker image.
 */

class pqWindTunnelContainerEngineSetup : public QObject
{
  Q_OBJECT
public:
  using Superclass = QObject;

  enum ContainerEngineStatus
  {
    Uninitialized = 0,
    Pending,
    Ready,
    Unavailable,
    Error
  };

  pqWindTunnelContainerEngineSetup(QWidget* parentWidget);
  ~pqWindTunnelContainerEngineSetup() = default;

  /** \brief Runs the setup/configuration logic
     *
     */
  bool start();

  QString containerEnginePath() const { return m_containerEnginePath; }
  ContainerEngineStatus status() const { return m_status; }
  QString statusAsString() const;

Q_SIGNALS:
  void finished();

public Q_SLOTS:

protected Q_SLOTS:
  void onProcessError(QProcess::ProcessError error);
  void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void onTryLsError();
  void pullDockerImage();

protected:
  enum ProcessMode
  {
    Undefined = 0,
    TryLs,
    TryPull
  };

  bool readSetting(QString& containerEnginePath) const;
  bool updateSetting(const QString& containerEnginePath) const;

  // Runs container "image ls" command  for openfoam docker image
  void tryLs(const QString& containerEnginePath);

  /** \brief methods getting/checking the docker executable */
  void askUserForPath(const QString& headline, const QString& details, QString& result) const;
  bool checkPath(const QString& path, QString& reason) const;
  bool getContainerEnginePath(QString& containerEnginePath) const;

  QString m_containerEnginePath;
  ContainerEngineStatus m_status = Uninitialized;
  ProcessMode m_processMode = Undefined;
  QProcess* m_process = nullptr;
  QWidget* m_parentWidget = nullptr;
  qtProgressDialog* m_progressDialog = nullptr;
};

#endif
