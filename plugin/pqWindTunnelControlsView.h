//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelControlsView_h
#define plugin_pqWindTunnelControlsView_h

#include "smtk/extension/qt/qtBaseView.h"

#include <QMap>
#include <QString>

class pqDataRepresentation;
class pqPipelineSource;

/** \brief A custom smtk view for presenting controls to the user.
 *
 * The controls include running an operation to generate OpenFOAM
 * input files, running the OpenFOAM application itself, setting
 * visibility for the resulting data, etc.
 * */

class pqWindTunnelControlsView : public smtk::extension::qtBaseView
{
  Q_OBJECT
public:
  smtkTypenameMacro(pqWindTunnelControlsView);

  static qtBaseView* createViewWidget(const smtk::view::Information& info);
  pqWindTunnelControlsView(const smtk::view::Information& info);
  virtual ~pqWindTunnelControlsView();

  bool isEmpty() const override;

  /** \brief Clears source map; should be called when project is closed. */
  static void clearSourceMap();

Q_SIGNALS:

public Q_SLOTS:
  /** \brief Update controls based on current project state.
     *
     * This is called each time the view is displayed, which is
     * typically when its parent tab is selected.
     */
  void updateUI() override;

protected Q_SLOTS:
  /** \brief Runs the operation and launches the foam application. */
  void onRunClicked();

  /** \brief Creates and runs operation dialog. */
  void runOperationDialog(smtk::operation::OperationPtr op);

  /** \brief Processes result from operation */
  void onOperationExecuted(smtk::attribute::AttributePtr result);

  /** \brief Called when the foam application completes */
  void onProcessFinished(qint64 pid, int exitcode);

  /** \brief Other user actions */
  void onDisplayClicked();
  void onRepresentationChanged();
  void onOpacityChanged(int i);
  void onHideOthersClicked();

protected:
  void createWidget() override;
  void updateLogControls();
  void updateGeometryControls();

  /** \brief Locates pqSMTKResource associated with given resource role
     *
     * Also inserts it into the map.
     * Note that pqSMTKResource is subclass of pqPipelineSource.
     */
  pqPipelineSource* locateSMTKResource(const QString& resourceRole);

  /** \brief Looks up representation, checking if its in the active view */
  pqDataRepresentation* getActiveRepresentation() const;

  /** \brief Looks up pipeline source in s_foamSourceMap or project resources */
  pqPipelineSource* getPipelineSource() const;

  /** \brief Finds or creates pipeline source for give foam file
     *
     *  Sets isNew true if source loaded from file.
     */
  pqPipelineSource* getPipelineSource(const QString& foamfile, bool& isNew);

  /** \brief Shared dictionary of pipeline sources keyed by string identifier
     *
     * Identifier is either foam file or resource role.
     */
  static QMap<QString, pqPipelineSource*> s_pipelineSourceMap;

private:
  class Internal;
  Internal* m_internal;
};

#endif
