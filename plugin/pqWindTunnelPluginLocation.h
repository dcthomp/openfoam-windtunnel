//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelPluginLocation_h
#define plugin_pqWindTunnelPluginLocation_h

#include <QObject>

#include <QDir>

class pqWindTunnelPluginLocation : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqWindTunnelPluginLocation(QObject* parent = nullptr);
  ~pqWindTunnelPluginLocation() override;

  void StoreLocation(const char* location);

protected:
  void locateWorkflows(const QDir& pluginResourcesDirectory) const;

private:
  Q_DISABLE_COPY(pqWindTunnelPluginLocation);
};

#endif
