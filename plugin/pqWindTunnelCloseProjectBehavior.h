//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqWindTunnelCloseProjectBehavior_h
#define plugin_pqWindTunnelCloseProjectBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for closing a project.
class pqWindTunnelCloseProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWindTunnelCloseProjectReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWindTunnelCloseProjectReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWindTunnelCloseProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWindTunnelCloseProjectBehavior* instance(QObject* parent = nullptr);
  ~pqWindTunnelCloseProjectBehavior() override;

  /** \brief Closes project, returning true if NOT canceled by the user
   *
   * In other words, the return value reports if the close action
   * was "not cancelled".
   */
  bool closeProject();

Q_SIGNALS:
  void projectClosed();

protected:
  pqWindTunnelCloseProjectBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindTunnelCloseProjectBehavior);
};

#endif
