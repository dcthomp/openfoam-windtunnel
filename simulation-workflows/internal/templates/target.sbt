<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>target</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Target">
      <Categories><Cat>target</Cat></Categories>
      <!-- <AssociationsDef Label=" " NumberOfRequiredValues="1" AdvanceLevel="0">
        <Accepts>
          <Resource Name="smtk::model::Resource" Filter="model" />
        </Accepts>
      </AssociationsDef> -->
      <ItemDefinitions>
        <!-- Store filename internally -->
        <String Name="Filename" Label="Asset Filename" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false" />

        <String Name="Edges" Label="Keep Edges">
          <ChildrenDefinitions>
            <Double Name="Angle" Label=" ">
              <DefaultValue>120.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="false">180.0</Max>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="No Edges">none</Value>
            <Structure>
              <Value Enum="Inner Angle">angle</Value>
              <Items><Item>Angle</Item></Items>
            </Structure>
            <Value Enum="All Edges">all</Value>
          </DiscreteInfo>
        </String>

        <Double Name="Tolerance" Label="surfaceFeature Tolerance">
          <DefaultValue>0.001</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Double Name="InsidePoint" Label="Inside Model Point" NumberOfRequiredValues="3">
          <BriefDescription>Specify point inside the target geometry to prevent meshing inside.</BriefDescription>
          <DefaultValue>0.5,0.0,0.0</DefaultValue>
        </Double>

      </ItemDefinitions>
    </AttDef>
  </Definitions>

</SMTK_AttributeResource>
