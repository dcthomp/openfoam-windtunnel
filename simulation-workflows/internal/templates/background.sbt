<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5" DisplayHint="true">
  <!-- ********** Background Mesh Specification ********** -->

  <Categories>
    <Cat>background</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Scale">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="scale">
          <BriefDescription>Length of 1 coordinate unit in meters.</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BoxWidget">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="box" Label="Geometry" NumberOfRequiredValues="6" >
          <!-- Values are xmin, xmax, ymin, ymax, zmin, zmax -->
          <DefaultValue>-2,3,0,1,-0.5,0.5</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BlockMeshSize">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <!-- <Int Name="NumberOfCells" Label="Mesh Cells" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>x:</Label>
            <Label> y:</Label>
            <Label> z:</Label>
          </ComponentLabels>
          <DefaultValue>50,10,10</DefaultValue>
        </Int> -->
        <String Name="MeshSize" Label="Mesh Size">
          <ChildrenDefinitions>
            <Int Name="numcells-maxdir" Label=" ">
              <DefaultValue>100</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <Double Name="relative-meshsize" Label=" ">
              <DefaultValue>0.01</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="false">1.0</Max>
              </RangeInfo>
            </Double>
            <Double Name="absolute-meshsize" Label=" ">
              <DefaultValue>0.05</DefaultValue>
              <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
            </Double>
            <Int Name="numcells-eachdir" Label=" " NumberOfRequiredValues="3">
              <DefaultValue>100,20,20</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Number of cells in longest direction">maxlen</Value>
              <Items><Item>numcells-maxdir</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Relative mesh size">relative-size</Value>
              <Items><Item>relative-meshsize</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Absolue mesh size">absolute-size</Value>
              <Items><Item>absolute-meshsize</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Number of cells in each direction">maxlen</Value>
              <Items><Item>numcells-eachdir</Item></Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <!-- Leave grading out (AdvanceLevel 99) -->
        <Int Name="simpleGrading" NumberOfRequiredValues="3" AdvanceLevel="99">
          <ComponentLabels>
            <Label>x:</Label>
            <Label> y:</Label>
            <Label> z:</Label>
          </ComponentLabels>
          <DefaultValue>1</DefaultValue>
        </Int>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BlockMeshBoundaryConditions">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <String Name="FrontBackSides" Label="Front and Back Sides">
          <BriefDescription>ZMin &amp; ZMax Sides</BriefDescription>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="Empty">empty</Value>
            <Value Enum="Zero Gradient">zeroGradient</Value>
            <Value Enum="Symmetry Plane">symmetryPlane</Value>
          </DiscreteInfo>
        </String>

        <String Name="TopSide" Label="Top Side">
          <BriefDescription>YMax Side</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Zero Gradient">zeroGradient</Value>
            <Value Enum="Wall">wall</Value>
          </DiscreteInfo>
        </String>

        <String Name="BottomSide" Label="Bottom Side">
          <BriefDescription>YMin Side</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Zero Gradient">zeroGradient</Value>
            <Value Enum="Wall">wall</Value>
          </DiscreteInfo>
        </String>

        <!-- More advance BCs not used -->
        <Group Name="boundary" Label="Boundaries" NumberOfRequiredGroups="1" AdvanceLevel="99">
          <ItemDefinitions>
            <String Name="left" Label="Left (-X)">
              <DiscreteInfo DefaultIndex="1">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
              </DiscreteInfo>
            </String>
            <String Name="right" Label="Right (+X)">
              <DiscreteInfo DefaultIndex="1">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
              </DiscreteInfo>
            </String>
            <String Name="bottom" Label="Bottom (-Y)">
              <DiscreteInfo DefaultIndex="2">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
              </DiscreteInfo>
            </String>
            <String Name="top" Label="Top (+Y)">
              <DiscreteInfo DefaultIndex="1">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
              </DiscreteInfo>
            </String>
            <String Name="back" Label="Back (-Z)">
              <DiscreteInfo DefaultIndex="1">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
                <Value>symmetryPlane</Value>
              </DiscreteInfo>
            </String>
            <String Name="front" Label="Front (+Z)">
              <DiscreteInfo DefaultIndex="1">
                <Value>empty</Value>
                <Value>patch</Value>
                <Value>wall</Value>
                <Value>symmetryPlane</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>
