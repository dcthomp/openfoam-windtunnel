<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">
  <Categories>
    <Cat>background</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="controlDict" Label="Controls">
      <Categories>
        <Cat>background</Cat>
      </Categories>
      <BriefDescription>Contents of controlDict</BriefDescription>
      <ItemDefinitions>
<!--         <String Name="application">
          <DiscreteInfo DefaultIndex="0">
            <Value>icoFoam</Value>
            <Value>simpleFoam</Value>
            <Value>pisoFoam</Value>
          </DiscreteInfo>
        </String> -->

        <String Name="startFrom">
          <ChildrenDefinitions>
            <Double Name="startTime" Label=" ">
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Value>firstTime</Value>
            <Structure>
              <Value>startTime</Value>
              <Items>
                <Item>startTime</Item>
              </Items>
            </Structure>
            <Value>latestTime</Value>
          </DiscreteInfo>
        </String>

        <String Name="stopAt">
          <ChildrenDefinitions>
            <Double Name="endTime" Label=" ">
              <DefaultValue>0.5</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value>endTime</Value>
              <Items>
                <Item>endTime</Item>
              </Items>
            </Structure>
            <Value>nextWrite</Value>
          </DiscreteInfo>
        </String>

        <Double Name="deltaT">
          <DefaultValue>0.005</DefaultValue>
        </Double>

        <String Name="writeControl">
          <ChildrenDefinitions>
            <Double Name="writeIntervalDouble" Label=" ">
              <DefaultValue>20.0</DefaultValue>
              <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
            </Double>
            <Int Name="writeIntervalInt" Label=" ">
              <DefaultValue>20</DefaultValue>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value>timeStep</Value>
              <Items><Item>writeIntervalInt</Item></Items>
            </Structure>
            <Structure>
              <Value>runTime</Value>
              <Items><Item>writeIntervalDouble</Item></Items>
            </Structure>
            <Structure>
              <Value>cpuTime</Value>
              <Items><Item>writeIntervalDouble</Item></Items>
            </Structure>
            <Structure>
              <Value>clockTime</Value>
              <Items><Item>writeIntervalDouble</Item></Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
