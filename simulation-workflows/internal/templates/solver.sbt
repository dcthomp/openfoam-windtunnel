<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>background</Cat>
    <Cat>target</Cat>
    <Cat>simpleFoam</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="PhysicalProperties" Unique="true">
      <Categories><Cat>target</Cat></Categories>
      <ItemDefinitions>
        <Double Name="KinematicViscosity" Label="Kinematic Viscosity (&#x3BD;)">
          <DefaultValue>0.01</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BoundaryCondition" Abstract="true">
      <Categories><Cat>target</Cat></Categories>
    </AttDef>

    <AttDef Type="PressureBoundaryCondition" BaseType="BoundaryCondition" Unique="true">
      <ItemDefinitions>
        <Double Name="Pressure" Label="Outlet Pressure">
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="VelocityBoundaryCondition" BaseType="BoundaryCondition" Unique="true" Version="1">
      <ItemDefinitions>
        <Double Name="Velocity" Label="Inlet Flow Velocity">
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="TurbulenceModel">
      <Categories><Cat>simpleFoam</Cat></Categories>
      <ItemDefinitions>
        <String Name="TurbulenceModel" Label="Turbulence Model">
          <ChildrenDefinitions>
            <String Name="RASModel" Label="RAS Model">
              <BriefDescription>Uses Reynolds-averaged stress modeling</BriefDescription>
              <ChildrenDefinitions>
                <Double Name="turbulentKE" Label="k">
                  <BriefDescription>Turbulent kinetic energy</BriefDescription>
                  <DefaultValue>0.375</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
                <Double Name="turbulentOmega" Label="omega">
                  <BriefDescription>Rate of dissipation</BriefDescription>
                  <DefaultValue>3.6</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
                <Double Name="turbulentEpsilon" Label="epsilon">
                  <BriefDescription>Rate of dissipation</BriefDescription>
                  <DefaultValue>0.12</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value>kEpsilon</Value>
                  <Items>
                    <Item>turbulentKE</Item>
                    <Item>turbulentEpsilon</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value>kOmega</Value>
                  <Items>
                    <Item>turbulentKE</Item>
                    <Item>turbulentOmega</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value>laminar</Value>
            <Structure>
              <Value>RAS</Value>
              <Items>
                <Item>RASModel</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>
