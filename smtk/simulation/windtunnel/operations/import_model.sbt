<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Import geometry file as the target to insert in the wind tunnel -->

  <Definitions>
    <AttDef Type="import-model" Label="Import Geometry" BaseType="operation">
      <BriefDescription>
        Import geometry file as the target to place in the wind tunnel.
      </BriefDescription>
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" ShouldExist="1" NumberOfRequiredValues="1" FileFilters="Supported Files (*.obj *.stl *.stlb)" />
        <Void Name="overwrite" Label="Overwrite?" Optional="true" IsEnabledByDefault="false" />
        <Void Name="import-vtp" Label="Import .vtp file?" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>vtkPolyData files sometimes import into mesh session when stl and obj dont</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::model::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
