class ProcessStatus:
    """Status of blockMesh call"""
    NotRun = 0
    Started = 1
    Completed = 2
    Error = 9
