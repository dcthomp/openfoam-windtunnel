<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Generic .sbt file for project operations -->
  <!-- Only load from operation which preloads smtk::operation base definitions) -->

  <Definitions>
    <AttDef Type="setup" BaseType="operation">
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="DoNotLock" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <String Name="run" Label="Run OpenFOAM application" Optional="true" IsEnabledByDefault="true">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Wait For Completion">sync</Value>
            <Value Enum="Launch And Return">async</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(setup)" BaseType="result">
      <ItemDefinitions>
        <Directory Name="case-directory" ShouldExist="true" />
        <Int Name="status"><DefaultValue>0</DefaultValue></Int>
        <Int Name="pid"><DefaultValue>0</DefaultValue></Int>
        <String Name="logfile"></String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
