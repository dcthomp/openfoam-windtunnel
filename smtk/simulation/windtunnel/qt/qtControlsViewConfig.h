//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_windtunnel_qt_qtControlsViewConfig_h
#define smtk_simulation_windtunnel_qt_qtControlsViewConfig_h

#include "smtk/simulation/windtunnel/qt/Exports.h"

#include <QObject>

#include <QString>
#include <QStringList>

#include <string>

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace simulation
{
namespace windtunnel
{

/** \brief Encapsulates data used in pqWindTunnelControlsView. */
class SMTKWINDTUNNELQTEXT_EXPORT qtControlsViewConfig : QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  qtControlsViewConfig(const smtk::view::ConfigurationPtr config, QObject* parent = nullptr);
  ~qtControlsViewConfig() = default;

  const QString& category() const { return m_category; }
  const QString& foamApplication() const { return m_foamApp; }
  const QStringList& foamArguments() const { return m_foamArguments; }
  const QString& caseDirectory();
  const QString& foamfileName() const { return m_foamfileName; }
  const QString& foamfilePath();
  const QString& resourceRole() const { return m_resourceRole; }
  const QString& logfilePath();
  const QString& defaultRepresentation() const { return m_defaultRepresentation; }

  const std::string& operationType() const { return m_operationType; }
  bool useOperationDialog() const { return m_useOperationDialog; }

  /** \brief Solver name can change at runtime */
  void setSolverName(const QString& solverName);

  void dump() const; // Writes values to qDebug()

protected:
  void initialize(const smtk::view::ConfigurationPtr config);

  enum GeometryType
  {
    None = 0,
    Resource,
    FoamFile
  };

  // Configuration data
  QString m_category;
  QString m_foamApp;
  QStringList m_foamArguments;
  QString m_caseDirectory;
  GeometryType m_geometryType = GeometryType::None; // needed?
  QString m_foamfileName;
  QString m_resourceRole;
  QString m_defaultRepresentation;

  std::string m_operationType;
  bool m_useOperationDialog = false;

  // Project-specific data
  QString m_caseName;
  QString m_foamfilePath;
  QString m_logfilePath;

private:
};

} // namespace windtunnel
} // namespace simulation
} // namespace smtk

#endif
