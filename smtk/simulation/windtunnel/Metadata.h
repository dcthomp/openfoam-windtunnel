//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_windtunnel_Metadata_h
#define smtk_simulation_windtunnel_Metadata_h

#include "smtk/simulation/windtunnel/Exports.h"

#include <string>

namespace smtk
{
namespace simulation
{
namespace windtunnel
{

/**\brief Metadata specific to WindTunnel projects.
 *
 * Includes both static constants used to across the code base and
 * instance data that are initialized by the application.
 */

struct SMTKWINDTUNNEL_EXPORT Metadata
{
  // Static constants - initialized in the cxx file
  static const std::string PROJECT_TYPE;            // "foam.windtunnel"
  static const std::string PROJECT_FILE_EXTENSION;  // ".project.smtk";
  static const std::string PROJECT_ATTRIBUTES_ROLE; // "attributes"
  static const std::string PROJECT_MODEL_ROLE;      // "model"

  // Directory containing windtunnel.sbt and windtunnel.py
  // Applications must set this!
  static std::string WORKFLOWS_DIRECTORY;

  // Directory containing python operations
  static std::string OPERATIONS_DIRECTORY;
};

} // namespace windtunnel
} // namespace simulation
} // namespace smtk

#endif
