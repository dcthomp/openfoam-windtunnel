# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================

"""Loads windtunnel operations. Run this script in modelbuilder python shell."""

import os
print('Loading', __file__)

import smtk
import smtk.attribute
import smtk.operation
import smtk.project
import smtk.resource
import smtk.extension.paraview.appcomponents as appcomps

PROJECT_NAME = 'foam.windtunnel'
OP_FILES = [
    'create_project.py',
    'blockmesh.py',
    'import_model.py',
    'surfaceextract.py',
    'refinemesh.py',
    'snappyhexmesh.py',
    'icofoam.py',
    'simplefoam.py',
]
OPS = [
    'create_project.CreateProject',
    'import_model.ImportModel',
    'blockmesh.BlockMesh',
    'surfaceextract.SurfaceFeatureExtract',
    'refinemesh.RefineMesh',
    'snappyhexmesh.SnappyHexMesh',
    'icofoam.IcoFoam',
    'simplefoam.SimpleFoam',
]


behavior = appcomps.pqSMTKBehavior.instance()
proj_manager = behavior.activeWrapperProjectManager()

# Register project type name
proj_manager.registerProject(PROJECT_NAME, set(), set(), '1.0.0')

# Import operations
op_manager = proj_manager.operationManager()
import_op = op_manager.createOperation('smtk::operation::ImportPythonOperation')

source_dir = os.path.abspath(os.path.dirname(__file__))
op_dir = os.path.abspath(os.path.join(source_dir, os.pardir,
                         'smtk/simulation/windtunnel/operations'))

for op in OPS:
    if op_manager.registered(op):
        print('Unregistering {}'.format(op))
        op_manager.unregisterOperation(op)

for op_file in OP_FILES:
    path = os.path.join(op_dir, op_file)
    import_op.parameters().findFile("filename").setValue(path)
    import_res = import_op.operate()
    import_outcome = import_res.findInt('outcome').value()
    print('{} import_outcome: {}'.format(op_file, import_outcome))

# Sanity checks
# is_registered = op_manager.registered('create_project.CreateProject')
# print('create_project is_registered:', is_registered)

# op_name = 'blockmesh.BlockMesh'
# blockmesh_op = op_manager.createOperation(op_name)
# print(blockmesh_op)
